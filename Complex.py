class complex:

    # Добавим конструктор, который выделяет память под объект
    def __init__(self, re, im):
        self.re = float(re)
        self.im = float(im)

    # Перегрузка печати - вызывается при печати объекта
    def __repr__(self):
        return '{} + i*{}\n'.format(self.re, self.im)

    def __str__(self):
        return '{} + i*{}\n'.format(self.re, self.im)

    # Перегрузка суммы компл. чисел
    def __add__(self, other):
        return complex(self.re + other.re, self.im + other.im)

    # перегрузка разности компл. чисел
    def __sub__(self, other):
        return complex(self.re - other.re, self.im - other.im)

    # Перегрузка сравнения(==) компл.чисел
    def __eq__(self, other):
        return self.re == other.re and self.im == other.im

    # Перегрузка сравнения(>) компл.чисел
    def __gt__(self, other):
        return self.re > other.re and self.im > other.im

    # Перегрузка сравнения(<) компл.чисел
    def __lt__(self, other):
        return self.re < other.re and self.im < other.im