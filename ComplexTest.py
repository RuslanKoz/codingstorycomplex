import unittest

from Complex import complex

class TestPoint(unittest.TestCase):

    def setUp(self):
        self.A = complex(300, 500)
        self.B = complex(6, 3)
        self.C = complex(0, 0)

    def test_init(self):
        self.assertEqual((self.A.re, self.A.im), (float(300), float(500)),
                         "Конструктор")

    def test_add(self):
        self.C = self.A + self.B

        re = self.A.re + self.B.re
        im = self.A.im + self.B.im

        self.assertTrue(self.C == complex(re, im), "Сумма верна")

    def test_sub(self):
        self.C = self.A - self.B

        re = self.A.re - self.B.re
        im = self.A.im - self.B.im

        self.assertTrue(self.C == complex(re, im), "Разность верна")

    def test_str(self):
        re = str(self.A.re)
        im = str(self.A.im)

        self.assertTrue(str(self.A) == '{} + i*{}\n'.format(self.A.re, self.A.im), "Печать")

    def test_eq(self):
        self.assertTrue(self.A == self.B, "Равны")

    def test_gt(self):
        self.assertTrue(self.A > self.B, " > ")

    def test_lt(self):
        self.assertTrue(self.A < self.B, " < ")

if __name__ == '__main__':
    unittest.main()